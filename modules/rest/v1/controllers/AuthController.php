<?php
namespace app\modules\rest\v1\controllers;

use Yii;
use yii\base\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\models\forms\LoginForm;

class AuthController extends Controller {
	// RBAC control
	public $freeAccess = true;

	public function behaviors() {
		return ArrayHelper::merge(parent::behaviors(), [
			'ghost-access'=> [
				'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
			],
		]);
	}
	
	public function actionIsGuest() {
		return Json::encode(Yii::$app->user->isGuest);
	}

	public function actionLogin() {
		$data = file_get_contents('php://input');
		if ($data === false) {
			return Json::encode(!Yii::$app->user->isGuest);	// error reading request data
		} else {	// good, lets validate
			$jsonData = json_decode($data);
			if (isset($jsonData->username) && isset($jsonData->password)) {
				$model = new LoginForm();
				$model->username = $jsonData->username;
				$model->password = $jsonData->password;
				$model->rememberMe = false;
				if ($model->validate()) {
					return Json::encode($model->login());
				} else {
					return Json::encode(false);
				}
			} else {
				return Json::encode(false);	// wrong data in request
			}
		}
	}
	
	public function actionLogout() {
		Yii::$app->user->logout();
	}

}