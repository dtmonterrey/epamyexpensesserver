<?php

use yii\db\Schema;
use yii\db\Migration;

class m150310_123905_create_category_table extends Migration
{
    public function up() {
    	$this->createTable("category", [
    			"id" 		=> Schema::TYPE_BIGINT . " PRIMARY KEY AUTO_INCREMENT",
    			"id_user" 	=> Schema::TYPE_INTEGER,
    			"name" 		=> Schema::TYPE_STRING,
    			"deleted" 	=> Schema::TYPE_BOOLEAN . " DEFAULT 0",
    			"pos" 		=> Schema::TYPE_SMALLINT . " DEFAULT 0",
    			"last_updated"=>Schema::TYPE_TIMESTAMP,
    	]);
    }

    public function down() {
    	$this->dropTable("category");
    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
