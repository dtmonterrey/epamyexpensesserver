<?php
namespace app\modules\rest\v1\controllers;

use app\models\Category;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\ServerErrorHttpException;
 
class CategoryController extends ActiveController {
    // We are using the regular web app modules:
    public $modelClass = 'app\models\Category';
    // RBAC control
    public $freeAccess = false;
    
    public function behaviors() {
    	return ArrayHelper::merge(parent::behaviors(), [
    		'ghost-access'=> [
    			'class' => 'webvimark\modules\UserManagement\components\GhostAccessControl',
    		],
    	]);
    }
    
    public function actions() {
    	$actions = parent::actions();
    	unset($actions['index']);
    	unset($actions['create']);
    	unset($actions['update']);
    	return $actions;
    }
    
    public function actionIndex() {
    	/* @var $modelClass \yii\db\BaseActiveRecord */
    	$modelClass = new Category();
    	return new ActiveDataProvider([
    			'query' => $modelClass::find()->where([
    			'id_user' => Yii::$app->user->id,
    		]),
    	]);
    }
    
    public function actionCreate() {
    	/* @var $model \yii\db\ActiveRecord */
    	$model = new Category();
    	
    	$model->load(Yii::$app->getRequest()->getBodyParams(), '');
    	$model->id_user = Yii::$app->user->id;
    	
    	if ($model->save()) {
    		$response = Yii::$app->getResponse();
    		$response->setStatusCode(201);
    		$id = implode(',', array_values($model->getPrimaryKey(true)));
    	} elseif (!$model->hasErrors()) {
    		throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
    	}
    	
    	return $model;
    }
    
    public function actionUpdate() {
    	$category = Yii::$app->getRequest()->getBodyParams();
    	/* @var $model ActiveRecord */
    	$model = Category::find()->where([
    			'id_user' => Yii::$app->user->id,
    			'pos' => $category['pos'],
    	])->one();
    	
    	if ($model === null) {
    		throw new ServerErrorHttpException('Category not found');
    	}
    	if (Yii::$app->user->id != $model->id_user) {
    		throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
    	}
    	
    	$model->load(Yii::$app->getRequest()->getBodyParams(), '');
    	
    	if ($model->save(true, ['name', 'pos']) === false && !$model->hasErrors()) {
    		throw new ServerErrorHttpException('Failed to update the object for unknown reason.');
    	}
    	
    	return $model;
    }
    
    
}