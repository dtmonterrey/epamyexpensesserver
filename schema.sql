CREATE TABLE category (
	id INTEGER PRIMARY KEY,
    category TEXT,
    enabled INTEGER(1) DEFAULT 0
);
