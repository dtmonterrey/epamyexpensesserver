<?php
// Check this namespace:
namespace app\modules\rest\v1;
 
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
 
        // ...  other initialization code ...
    }
}