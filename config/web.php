<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'DuVM3oHoQdaouLtmVIyAy8nkczXKyA2R',
        	// Enable JSON Input:
        	'parsers' => [
        		'application/json' => 'yii\web\JsonParser',
        	]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
        	'class' => 'webvimark\modules\UserManagement\components\UserConfig',
            'enableAutoLogin' => true,
        	'on afterLogin' => function($event) {
        		\webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
        	}
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
        	'enablePrettyUrl' => true,
        	'enableStrictParsing' => false,
        	'showScriptName' => false,
        	'ruleConfig' => ['class' => 'yii\web\UrlRule'],
        	'rules' => [
        		['class' => 'yii\rest\UrlRule', 'controller' => 'rest-v1/category', 'pluralize'=>false],
        		['class' => 'yii\rest\UrlRule', 'controller' => 'category', 'pluralize'=>false],
        	],
        ]
    ],
    'params' => $params,
	'modules' => [
		'user-management' => [
			'class' => 'webvimark\modules\UserManagement\UserManagementModule',
			'on beforeAction' => function(yii\base\ActionEvent $event) {
				if ($event->action->uniqueId == 'user-management/auth/login') {
					$event->action->controller->layout = 'loginLayout.php';
				};
			},
		],
		'rest-v1' => [
			'class' => 'app\modules\rest\v1\Module',
		],
	],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
